# Exercise1
Коллекции и дженерики

#Содержание файла "Track.java"

public class Track {

	public Track() {
		
	}

}

#Содержание файла "CompactDisc.java"

public class CompactDisc extends Media {
	
	public CompactDisc() {
	
	}
	
}

#Содержание файла "Library.java"

public class Library<T> {

	public Library() {
		
	}

}

#Инструкция по запуску проекта

#Скопируйте указанный ниже код
#Вставьте с ПОЛНОЙ заменой в файл tasks.json
#Расположение файла tasks.json: Explorer => первая папка ".theia" => tasks.json

{
    "tasks": [
        {
            "type": "che",
            "label": "OnlineMedia build and run",
            "command": "javac -sourcepath /projects/Lab1/Collection/src -d /projects/Lab1/Collection/bin /projects/Lab1/Collection/src/*.java && java -classpath /projects/Lab1/Collection/bin OnlineMedia",
            "target": {
                "workingDir": "${CHE_PROJECTS_ROOT}/Lab1/Collection/src",
                "component": "maven"
            }
        }
    ]
}
